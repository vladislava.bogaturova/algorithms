Basic types of Data Structures

As we have discussed above, anything that can store data can be called as a data structure, hence Integer, Float, Boolean, Char etc, all are data structures. They are known as Primitive Data Structures.

Then we also have some complex Data Structures, which are used to store large and connected data. Some example of Abstract Data Structure are :
-Linked List
-Tree
-Graph
-Stack, Queue etc.


Linked Lists:   Linked List is a very commonly used linear data structure which consists of group of nodes in a sequence.
                Each node holds its own data and the address of the next node hence forming a chain like structure.
                Linked Lists are used to create trees and graphs.

Binary Trees:   A binary tree is a hierarchical data structure in which each node has at most two children generally referred as left child and right child.
                Each node contains three components:
                Pointer to left subtree
                Pointer to right subtree
                Data element
                The topmost node in the tree is called the root. An empty tree is represented by NULL pointer.
                
 Stack:         is an abstract data type with a bounded(predefined) capacity. It is a simple data structure that allows adding and removing elements in a 
                particular order.   Every time an element is added, it goes on the top of the stack and the only element that can be removed is the element that is at the top of the stack, just like a pile of objects.               